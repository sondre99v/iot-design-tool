# Design tool components
- Frontend interface
- Database of components
- Simulator
- Search and optimization engine

## Frontend interface
Allows an engineer to input part choices, requirements, environment parameters
etc., run searches and simulations, and evaluate suggested designs. Also
options to supplement database with custom components.

## Database of components
Contains parameterized models of various types of components.



## Simulator
Takes a specific configuration and environment, and simulates the operation of
the system, possibly multiple times with different seeds. Then it calculates
metrics and statistics for evaluation.

## Search and optimization engine
Will run simulations for various system configurations to find a feasible and
optimal solution.


# Technology for development
- See tutorial: "Dockerizing Django with Postgres, Gunicorn, and Nginx"
- Host on Amazon Web Services?
